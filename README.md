# Maple

Maple is a simple property mapper, it maps an array to an object, through its public properties or setters.

## Design choices / Philosophy

1. Maple is totally storage agnostic. It does not (nor wants to) know about your backend.
1. Maple is configured through PHP code. No Annotations, XML, YML or whatever.
1. Maple will only map properties found in the passed data set. It will not trigger errors for missing and/or extra properties.

## Running the tests

The test suite uses composer's autoloader. To run tests from the project's root dir:

```
curl -s http://getcomposer.org/installer | php
php composer.phar install
phpunit
```

## Builtin transformers

* `Alias($name)`: returns the value of the `$name` field.
* `Coalesce($callable, ...)`: returns the first non-null transformation.
* `Collection($name, $mapper)`: maps each item of an array.
* `Group($prefix)`: group fields by prefix.
* `Pipe($callable, ...)`: pipe any number of transformations.
* `Relation($name, $prefix, $mapper)`: composite transformer to handle relations.

## Usage
```php
<?php

// Simple property map
$data = ['title' => 'Fondation'];

// Instead of passing a class name, which would make things complicated as soon as your
// mapped objects require a bit of setup, we directly pass an instance to the constructor
// That instance is just a prototype and will be cloned for each mapping so your $mapper
// is actually re-usable ad nauseam
$mapper = new Maple\Mapper(new Model\Book());

// Mappers actually implement the TransformerInterface interface
// So they can freely be used wherever a Transformer is expected
$book = $mapper->transform($data);

echo $book->title; // 'Fondation'

// Has inflection by default
// Maple comes with a simple underscore to camelCase inflector
$data = ['published_at' => 1957];
$mapper = new Maple\Mapper(new Model\Book());

// All built-in transformers implement the __invoke magic method
$book = $mapper($data);

echo $book->publishedAt; // 1957

// Custom inflection (any callable will do)
$mapper = new Maple\Mapper(new Model\Book(), [], ['inflector' => function($string) {
    return strtoupper($string);
}]);

// Or just disable inflection alltogether
$mapper = new Maple\Mapper(new Model\Book(), [], ['inflector' => null]);

// Nested mapping through transformers
$data = [
    'title' => 'Fondation',
    'author__name' => 'Isaac Asimov',
];

$mapper = new Maple\Mapper(new Model\Book(), [
    'author' => new Maple\Transformer\Relation('author', 'author__', new Maple\Mapper(new Model\Author()))
]);

$book = $mapper($data);

echo $book->author->name; // 'Isaac Asimov'

// Transformers can also add virtual fields, based on others
$data = ['title' => 'Fondation'];
$mapper = new Maple\Mapper(new Model\Book(), [
    'abbr_title' => function($data) { return substr($data['title'], 0, 10); },
]);

// Actually, the transformers are useful for a lot of things
$mapper = new Maple\Mapper(new Model\Book(), [
    'title' => new Alias('some_completely_unrelated_but_legacy_name'),
    'created_at' => function($data) { return new DateTime($data['created_at']); }
]);

// And you can pipe them
// (This is the actual implementation of the Relation Transformer)
use Maple\Transformer\Pipe;
use Maple\Transformer\Group;

$mapper = new Maple\Mapper(new Model\Book(), [
    'author' => new Pipe(
        new Coalesce(new Alias('author'), new Group('author__')),
        new Maple\Mapper(new Model\Author())
    ),
]);

// Cascading inflection is not supported, sorry
// Because I did not find a clean way to do it, feel free to suggest
$inflector = new Maple\Inflector();
$authorMapper = new Maple\Mapper(new Model\Author(), [], ['inflector' => $inflector])

$mapper = new Maple\Mapper(new Model\Book(), [
    'author' => new Maple\Transformer\Group('author__')
], ['inflector' => $inflector]);

// But you can still prepare your options once and for all somewhere
$options = ['inflector' => new Inflector()];
$authorMapper = new Maple\Mapper(new Model\Author(), [], $options);
$mapper = new Maple\Mapper(new Model\Book(), ['author' => $authorMapper]);

// Last but not least, you can even encapsulate stuff
class BookMapper extends Maple\Mapper
{
    public function __construct($options = array())
    {
        $options = array_replace(['inflector' => new Inflector()], $options);

        parent::__construct(new Model\Book(), [
            'author' => new Maple\Transformer\Group('author__', new AuthorMapper($options))
        ], $options);
    }
}

$mapper = new BookMapper();
$book = $mapper($data);
```