<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Maple\Transformer;

use PHPUnit_Framework_TestCase;

use Maple\Transformer\Alias;

class Book
{
    public $title;
}

class AliasTest extends PHPUnit_Framework_TestCase
{
    public function testReturnsRightField()
    {
        $data = ['silly_field_name' => 'Fondation'];
        $alias = new Alias('silly_field_name');

        $this->assertEquals($data['silly_field_name'], $alias->transform($data));
    }

    public function testReturnsNullIfNothingFound()
    {
        $alias = new Alias('foo');
        $data = ['title' => 'Fondation'];

        $this->assertEquals(null, $alias($data));
    }
}