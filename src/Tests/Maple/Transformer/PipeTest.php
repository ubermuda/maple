<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Maple;

use PHPUnit_Framework_TestCase;

use Maple\Transformer\Pipe;

class PipeTest extends PHPUnit_Framework_TestCase
{
    public function testPipesTransformers()
    {
        $t = new Pipe(
            function($data) { return 'foo'; },
            function($data) { return strtoupper($data); }
        );

        $this->assertEquals('FOO', $t([]));
    }
}