<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Maple;

use PHPUnit_Framework_TestCase;

use Maple\Transformer\Collection;
use Maple\Mapper;

class CollAuthor
{
    public $name;
}

class CollectionTest extends PHPUnit_Framework_TestCase
{
    public function testMapsArray()
    {
        $t = new Collection('authors', new Mapper(new CollAuthor()));
        $data = [
            'authors' => [
                ['name' => 'Isaac Asimov'],
                ['name' => 'Franck Herbert']
            ]
        ];

        $coll = [];

        $author = new CollAuthor();
        $author->name = 'Isaac Asimov';
        $coll[] = $author;

        $author = new CollAuthor();
        $author->name = 'Franck Herbert';
        $coll[] = $author;

        $this->assertEquals($coll, $t($data));
    }
}