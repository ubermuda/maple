<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Maple;

use PHPUnit_Framework_TestCase;

use Maple\Transformer\Coalesce;

class CoalesceTest extends PHPUnit_Framework_TestCase
{
    public function testReturnsFirstNonNullValue()
    {
        $t = new Coalesce(
            function() { return null; },
            function() { return 'foo'; },
            function() { return 'bar'; }
        );

        $this->assertEquals('foo', $t([]));
    }

    public function testDontFuckDataSet()
    {
        $t = new Coalesce(
            function($data) { return null; },
            function($data) { return $data; }
        );

        $this->assertEquals([], $t([]));
    }
}