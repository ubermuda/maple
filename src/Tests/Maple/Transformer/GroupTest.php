<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Maple\Transformer;

use PHPUnit_Framework_TestCase;

use Maple\Mapper;
use Maple\Transformer\Group;

class Author
{
    public $name;

    public $style;
}

class GroupTest extends PHPUnit_Framework_TestCase
{
    public function testGroupsData()
    {
        $group = new Group('author__');

        $data = [
            'author__name'  => 'Isaac Asimov',
            'author__style' => 'Science Fiction',
        ];

        $expect = [
            'name'  => 'Isaac Asimov',
            'style' => 'Science Fiction'
        ];

        $this->assertEquals($expect, $group->transform($data));
    }

    public function testReturnsNullIfNothingFound()
    {
        $group = new Group('author__');
        $data = ['title' => 'Fondation'];

        $this->assertEquals(null, $group($data));
    }
}