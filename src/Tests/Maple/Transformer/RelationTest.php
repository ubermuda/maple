<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Maple;

use PHPUnit_Framework_TestCase;

use Maple\Transformer\Relation;
use Maple\Mapper;

class SimpleAuthor
{
    public $name;
}

class RelationTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testMapsRelation($data)
    {
        $t = new Relation('author', 'author__', new Mapper(new SimpleAuthor()));

        $author = new SimpleAuthor();
        $author->name = 'Isaac Asimov';

        $this->assertEquals($author, $t($data));
    }

    public function dataProvider()
    {
        return [
            [
                ['author' => ['name' => 'Isaac Asimov']]
            ],
            [
                ['author__name' => 'Isaac Asimov']
            ]
        ];
    }
}