<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Maple\Inflector;

use PHPUnit_Framework_TestCase;
use StdClass;

use Maple\Inflector\Inflector;

class InflectorTest extends PHPUnit_Framework_TestCase
{
    public function testIsInvokable()
    {
        $inflector = new Inflector();
        $this->assertEquals('fooBar', call_user_func($inflector, 'foo_bar'));
    }

    public function testEmptyString()
    {
        $inflector = new Inflector();
        $this->assertEquals('', $inflector->inflect(''));
    }

    public function testTransformsUnderscoresToLowerCamelCase()
    {
        $inflector = new Inflector();
        $this->assertEquals('fooBar', $inflector->inflect('foo_bar'));
    }

    /**
     * @dataProvider nonStringValuesProvider
     * @expectedException \InvalidArgumentException
     */
    public function testFailsOnNonStringArgument($value)
    {
        $inflector = new Inflector();
        $inflector->inflect($value);
    }

    public function nonStringValuesProvider()
    {
        return [[[]], [1], [new StdClass()]];
    }
}