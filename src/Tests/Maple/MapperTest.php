<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Maple;

use PHPUnit_Framework_TestCase;

use ArrayIterator;
use StdClass;

use Maple\Mapper;
use Maple\Inflector\Inflector;
use Maple\Transformer\Group;
use Maple\Transformer\Alias;

class Book
{
    public $title;

    public $pages_number;

    public $excerpt;

    public $authorName;

    public $author;

    public $publishedAt;

    private $realPublishedAt;

    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }
}

class Author
{
    public $name;

    public $style;
}

class MapperTest extends PHPUnit_Framework_TestCase
{
    public function testMapToProperties()
    {
        $data = [
            'title' => 'Fondation',
            'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ];

        $mapper = new Mapper(new Book());

        $book = new Book();
        $book->title = $data['title'];
        $book->excerpt = $data['excerpt'];

        $this->assertEquals($book, $mapper->transform($data));
    }

    public function testMapToSetter()
    {
        $data = ['publishedAt' => 1957];
        $mapper = new Mapper(new Book());

        $book = new Book();
        $book->setPublishedAt(1957);

        $this->assertEquals($book, $mapper($data));
    }

    public function testMapToSetterBeforeProperty()
    {
        $data = ['publishedAt' => 1957];
        $mapper = new Mapper(new Book());

        $book = new Book();
        $book->setPublishedAt(1957);

        $this->assertEquals($book, $mapper($data));
    }

    public function testIsInvokable()
    {
        $data = ['title' => 'Fondation'];
        $mapper = new Mapper(new Book());

        $book = new Book();
        $book->title = $data['title'];

        $this->assertEquals($book, $mapper($data));
    }

    public function testHasDefaultInflector()
    {
        $data = ['author_name' => 'Isaac Asimov'];

        $mapper = new Mapper(new Book());

        $book = new Book();
        $book->authorName = $data['author_name'];

        $this->assertEquals($book, $mapper($data));
    }

    public function testCanDisableInflector()
    {
        $data = ['pages_number' => 255];
        $mapper = new Mapper(new Book(), [], ['inflector' => null]);

        $book = new Book();
        $book->pages_number = $data['pages_number'];

        $this->assertEquals($book, $mapper($data));
    }

    /**
     * @expectedException PHPUnit_Framework_Error
     */
    public function testRejectsInvalidInflector()
    {
        $mapper = new Mapper(new Book(), [], ['inflector' => []]);
    }

    public function testUsesGroupTransformer()
    {
        $data = [
            'title' => 'Fondation',
            'author__style' => 'Science Fiction',
        ];

        $book = new Book();
        $book->title = 'Fondation';
        $book->author = ['style' => $data['author__style']];

        $mapper = new Mapper(new Book(), [
            'author' => new Group('author__'),
        ]);

        $this->assertEquals($book, $mapper->transform($data));
    }

    public function testAcceptsTraversableObject()
    {
        $data = new ArrayIterator(['title' => 'Fondation']);
        $mapper = new Mapper(new Book());

        $book = new Book();
        $book->title = 'Fondation';

        $this->assertEquals($book, $mapper($data));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testDoesNotAcceptAnythingElse()
    {
        $data = new StdClass();
        $mapper = new Mapper(new Book());

        $mapper($data);
    }

    public function testUsesAliasesIfProvided()
    {
        $data = ['silly_field_name' => 'Fondation'];
        $mapper = new Mapper(new Book(), ['title' => new Alias('silly_field_name')]);

        $book = new Book();
        $book->title = $data['silly_field_name'];

        $this->assertEquals($book, $mapper($data));
    }

    public function testAcceptsAnyCallableAsTransformer()
    {
        $data = ['title' => 'fondation'];
        $mapper = new Mapper(new Book(), ['title' => function($data) { return ucfirst($data['title']); }]);

        $book = new Book();
        $book->title = 'Fondation';

        $this->assertEquals($book, $mapper($data));
    }

    public function testTransformersReturningNull()
    {
        $data = ['title' => 'fondation'];
        $mapper = new Mapper(new Book(), ['author' => new Group('author__')]);

        $book = new Book();
        $book->title = $data['title'];

        $this->assertEquals($book, $mapper($data));
    }
}