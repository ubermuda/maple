<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Maple\Transformer;

use Maple\Transformer\Pipe;
use Maple\Transformer\Coalesce;
use Maple\Transformer\Alias;
use Maple\Transformer\Group;
use Maple\Mapper;

/**
 * @package Maple
 */
class Relation implements TransformerInterface
{
    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string
     */
    private $prefix;

    /**
     * @var Maple\Mapper
     */
    private $mapper;

    /**
     * @param string $name
     * @param string $prefix
     * @param Maple\Mapper $mapper
     */
    public function __construct($name, $prefix, Mapper $mapper)
    {
        $this->name = $name;
        $this->prefix = $prefix;
        $this->mapper = $mapper;

        $this->transformer = new Pipe(
            new Coalesce(
                new Alias($this->name),
                new Group($this->prefix)
            ),
            $this->mapper
        );
    }

    /**
     * @param array $data
     * @return object
     */
    public function transform($data)
    {
        return call_user_func($this->transformer, $data);
    }

    /**
     * @param array $data
     * @return object
     */
    public function __invoke($data)
    {
        return $this->transform($data);
    }
}