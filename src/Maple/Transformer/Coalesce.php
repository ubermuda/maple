<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Maple\Transformer;

/**
 * @package Maple
 */
class Coalesce implements TransformerInterface
{
    /**
     * @var array
     */
    private $transformers;

    /**
     * @param callable... A list of callables
     */
    public function __construct()
    {
        $this->transformers = func_get_args();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function transform($data)
    {
        foreach ($this->transformers as $transformer) {
            if (null !== $tdata = $transformer($data)) {
                return $tdata;
            }
        }

        return null;
    }

     /**
     * @param array $data
     * @return mixed
     */
   public function __invoke($data)
    {
        return $this->transform($data);
    }
}