<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Maple\Transformer;

use InvalidArgumentException;

/**
 * @package Maple
 */
class Group implements TransformerInterface
{
    /**
     * @var string
     */
    private $prefix;

    /**
     * @var callable
     */
    private $mapper;

    /**
     * @param string $prefix
     * @param callable $mapper
     */
    public function __construct($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @param array|Traversable $data
     * @return array|object
     */
    public function transform($data)
    {
        if (!is_array($data) && !$data instanceof Traversable) {
            throw new InvalidArgumentException(sprintf('Expected an array or instance of Traversable, got "%s"',
                is_object($data) ? get_class($data) : gettype($data)
            ));
        }

        $group = [];
        $prefixLength = strlen($this->prefix);

        foreach ($data as $key => $value) {
            if (0 === strpos($key, $this->prefix)) {
                $group[substr($key, $prefixLength)] = $value;
            }
        }

        if (0 === count($group)) {
            return null;
        }

        return $group;
    }

    /**
     * @param array|Traversable $data
     * @return array|object
     */
    public function __invoke($data)
    {
        return $this->transform($data);
    }
}