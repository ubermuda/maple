<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Maple\Transformer;

use Maple\Mapper;

/**
 * @package Maple
 */
class Collection implements TransformerInterface
{
    /**
     * @var Maple\Mapper
     */
    private $mapper;

    /**
     * @param string $field
     * @param string $prefix
     * @param Maple\Mapper $mapper
     */
    public function __construct($field, Mapper $mapper)
    {
        $this->field = $field;
        $this->mapper = $mapper;
    }

    /**
     * @param array $data
     * @return object
     */
    public function transform($data)
    {
        if (!isset($data[$this->field])) {
            return null;
        }
        
        return array_map($this->mapper, $data[$this->field]);
    }

    /**
     * @param array $data
     * @return object
     */
    public function __invoke($data)
    {
        return $this->transform($data);
    }
}