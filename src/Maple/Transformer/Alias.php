<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Maple\Transformer;

/**
 * @package Maple
 */
class Alias implements TransformerInterface
{
    /**
     * @var string
     */
    private $field;

    /**
     * @param string $field
     */
    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * @param array $data
     * @return string
     */
    public function transform($data)
    {
        if (!isset($data[$this->field])) {
            return null;
        }
        
        return $data[$this->field];
    }

    /**
     * @param array $data
     * @return string
     */
    public function __invoke($data)
    {
        return $this->transform($data);
    }
}