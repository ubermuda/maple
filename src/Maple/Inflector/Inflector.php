<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Maple\Inflector;

use InvalidArgumentException;

/**
 * @package Maple
 */
class Inflector
{
    /**
     * @param string $string
     */
    public function inflect($string)
    {
        if (!is_string($string)) {
            throw new InvalidArgumentException(sprintf('expected a string, got a "%s"', gettype($string)));
        }

        if (0 === strlen($string) || false === strpos($string, '_')) {
            return $string;
        }

        return lcfirst(str_replace(' ', '', ucwords(strtr(strtolower($string), '_', ' '))));
    }

    /**
     * @param string $string
     */
    public function __invoke($string)
    {
        return $this->inflect($string);
    }
}