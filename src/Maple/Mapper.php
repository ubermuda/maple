<?php

/**
 * This file is part of the Maple package
 * 
 * (c) Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Maple;

use InvalidArgumentException;
use Traversable;

use Maple\Inflector\Inflector;
use Maple\Transformer\TransformerInterface;

/**
 * @package Maple
 */
class Mapper implements TransformerInterface
{
    /**
     * @var object
     */
    private $prototype;

    /**
     * @var callable
     */
    private $inflector;

    /**
     * @var array[]callable
     */
    private $transformers;

    /**
     * @param object            $prototype
     * @param array[]callable   $transformers
     * @param callable          $inflector
     */
    public function __construct($prototype, array $transformers = array(), array $options = array())
    {
        $this->prototype = $prototype;
        $this->transformers = $transformers;
        $this->options = $options;

        $this->setInflector(array_key_exists('inflector', $options)
            ? $options['inflector']
            : new Inflector()
        );
    }

    public function setInflector(callable $inflector = null)
    {
        $this->inflector = $inflector;
    }

    /**
     * Maps data. Yay.
     * 
     * @param array|Traversable $data
     * @return object
     */
    public function transform($data)
    {
        $class = get_class($this->prototype);

        if ($data instanceof $class) {
            return $data;
        }

        if (!is_array($data) && !$data instanceof Traversable) {
            throw new InvalidArgumentException(sprintf('Expected an array or instance of Traversable, got "%s"',
                is_object($data) ? get_class($data) : gettype($data)
            ));
        }

        $object = clone $this->prototype;

        foreach ($this->transformers as $name => $callable) {
            $value = call_user_func($callable, $data);

            if (null !== $value) {
                $data[$name] = $value;
            }
        }

        foreach ($data as $key => $value) {
            $property = null === $this->inflector ? $key : call_user_func($this->inflector, $key);
            $method = 'set'.ucfirst($property);

            if (method_exists($object, $method)) {
                call_user_func([$object, $method], $value);
            } elseif (property_exists($object, $property)) {
                $object->$property = $value;
            }
        }

        return $object;
    }

    /**
     * @param array|Traversable
     * @return object
     */
    public function __invoke($data)
    {
        return $this->transform($data);
    }
}